import React from 'react';
import {Table , Breadcrumb ,Card} from 'antd'
import SearchForm from './SearchForm'
import 'whatwg-fetch'


const columns = [
  { title: '名称', width: 250, dataIndex: 'name', key: 'name',fixed: 'left'},
  { title: '分类ID', width: 100, dataIndex: 'age', key: 'age'},
  { title: '是否推荐', dataIndex: 'address', key: '1', width: 200 },
  { title: '排序', dataIndex: 'address', key: '2', width: 200 },
  { title: '级别', dataIndex: 'address', key: '3', width: 200 },
  { title: '可选值', dataIndex: 'address', key: '4'},
  {
    title: 'Action',
    key: 'operation',
    fixed: 'right',
    width: 100,
    render: () => <a href="#">action</a>,
  },
];

const data = [];
for (let i = 0; i < 100; i++) {
  data.push({
    key: i,
    name: `Edrward ${i}`,
    age: 32,
    address: `London Park no. ${i}`
  });
}

class CategoryListPage extends React.Component {

	state = {
	    data: [],
	    pagination: {},
	    loading: false,
	};

	constructor(props) {
		super(props);
	}

	handleTableChange = (pagination, filters, sorter) => {
		this.fetchData({pagination , filters , sorter});
	}

	fetchData = (params = {}) => {
		this.setState({loading:true});
		var self = this;
		fetch('' , params).then((response) => {
			setTimeout(function(){
				self.setState({
					loading : false,
					data : response.data
				})
			} , 1000)
			
		})
	}

	componentDidMount(){
		this.fetchData();
	}

	render () {
		return (
			<div id="App-category-list">
				<Breadcrumb className="App-breadcrumb">
                    <Breadcrumb.Item>首页</Breadcrumb.Item>
                    <Breadcrumb.Item>分类管理</Breadcrumb.Item>
                    <Breadcrumb.Item>分类列表</Breadcrumb.Item>
                </Breadcrumb>
				<Card bordered={false}>
					<SearchForm/>
					<Table columns={columns} 
						   size="large" 
						   bordered 
						   dataSource={data} 
						   scroll={{x: '130%'}}
						   loading={this.state.loading}
						   onChange={this.handleTableChange} />
				</Card>
			</div>
			)
	}
}

export default CategoryListPage;