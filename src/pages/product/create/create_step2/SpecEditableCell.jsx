import React from 'react';
import {Form,Input} from 'antd';
import EditableCell from 'components/EditableTable/EditableCell';

class SpecEditableCell extends EditableCell {

    render () {
        const { value, editable } = this.state;
        const formItemLayout = {
            labelCol: {
                xs: {span: 20},
                sm: {span: 3},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 14},
            },
        };
        const {getFieldDecorator} = this.props.form;
        const {columnDef} = this.props.columnDef;
        return (
            <div>
                <Form.Item {...formItemLayout}>
                    {
                        editable ?
                            <div>
                                {getFieldDecorator(`products[${this.props.prodIndex}][${this.props.dataIndex}]` , {
                                    initialValue : value.toString()
                                })(
                                    <Input
                                        onChange={e => this.handleChange(e)}
                                    />
                                )}
                            </div>
                            :
                            <div>
                                {getFieldDecorator(`products[${this.props.prodIndex}][${this.props.dataIndex}]` , {
                                    initialValue : value.toString()
                                })(
                                    <Input type='hidden'/>
                                )}
                                <span>{value.toString() || ''}</span>
                            </div>


                    }
                </Form.Item>
            </div>
        );
    }

}

export default SpecEditableCell;