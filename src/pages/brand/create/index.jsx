import React from 'react';
import {Form, Input, Tooltip, Icon, Cascader, Select, Table, Upload, Checkbox, Button, Card , Breadcrumb , Modal} from 'antd';
import BrandCreateForm from './form'

class BrandCreatePage extends React.Component {

    state = {
        data : []
    }

    render() {

        return (
            <div>
                <Breadcrumb className="App-breadcrumb">
                    <Breadcrumb.Item>首页</Breadcrumb.Item>
                    <Breadcrumb.Item>品牌管理</Breadcrumb.Item>
                    <Breadcrumb.Item>创建品牌</Breadcrumb.Item>
                </Breadcrumb>
                <Card bordered={false}>
                    <BrandCreateForm data={this.state.data}/>
                </Card>
            </div>
        );
    }
}

export default BrandCreatePage;