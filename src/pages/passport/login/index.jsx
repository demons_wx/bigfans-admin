import React from 'react';
import { Form, Icon, Input, Button, Checkbox ,Row , Col , message} from 'antd';

import HttpUtils from 'utils/HttpUtils'
import StorageUtils from 'utils/StorageUtils'

const FormItem = Form.Item;

class NormalLoginForm extends React.Component {

  constructor(props){
    super(props)
  }

  handleSubmit(e) {
    let self = this;
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        HttpUtils.login(values , {
          success(resp){
            StorageUtils.setToken(resp.data)
            self.props.history.push('/')
          }
        })
      }
    });
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Row>
        <Col span={8} offset={10}>
          <Form onSubmit={(e) => this.handleSubmit(e)} className="login-form">
          <FormItem>
            {getFieldDecorator('username', {
              rules: [{ required: true, message: 'Please input your username!' }],
            })(
              <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
            )}
          </FormItem>
          <FormItem>
            {getFieldDecorator('password', {
              rules: [{ required: true, message: 'Please input your Password!' }],
            })(
              <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
            )}
          </FormItem>
          <FormItem>
            {getFieldDecorator('remember', {
              valuePropName: 'checked',
              initialValue: true,
            })(
              <Checkbox>Remember me</Checkbox>
            )}
            <a className="login-form-forgot" href="">Forgot password</a>
            <Button type="primary" htmlType="submit" className="login-form-button">
              Log in
            </Button>
            <span> 测试账号: admin / asdf </span>
          </FormItem>
        </Form>
        </Col>

      </Row>
      
    );
  }
}

const WrappedNormalLoginForm = Form.create()(NormalLoginForm);
export default WrappedNormalLoginForm;